import Vue from 'vue'
import App from './App.vue'
import Vuex from 'vuex';
import VueRouter from 'vue-router'
import axios from 'axios'
import VueAxios from 'vue-axios'

import router from "./router";
import store from "./store";

Vue.use(Vuex, VueRouter, VueAxios, axios)
Vue.config.productionTip = false

new Vue({
  render: h => h(App),
  router,
  store,
}).$mount('#app')
