import Vue from "vue";
import Router from "vue-router";
import Payment from "@/pages/Payment";
import User from "@/pages/User";

Vue.use(Router);

export default new Router({
  mode: "history",
  routes: [
    { path: "/", redirect: "/payment" },
    {
      path: "/payment",
      name: "Payment",
      component: Payment
    },
    {
      path: "/user",
      name: "User",
      component: User,
    },
  ],
  scrollBehavior() {
    return { x: 0, y: 0 };
  }
});
