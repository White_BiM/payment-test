import axios from "axios";
const HTTP = axios.create({
  baseURL: "http://www.mocky.io/v2/5eaffb6c3300005c00c68a8a"
});

export default {
  state: {
    user: null
  },
  mutations: {
    setUser(state, payload) {
      state.user = payload;
    }
  },

  actions: {
    async fetchUser({ commit }) {
      commit("setLoading", true);
      try {
        let user = {};
        await HTTP.get().then(response => {
          user = response.data;
          console.log(user)
        });
        commit("setUser", user);
        commit("setLoading", false);
      } catch (error) {
        commit("setLoading", false);
        throw error;
      }
    },
  },

  getters: {
    user(state) {
      return state.user;
    }
  }
};
