import { Swal } from "../sweet-alert";

export default {
  state: {
    loading: false,
    sum: 200,
  },
  mutations: {
    setLoading(state, payload) {
      state.loading = payload;
    },
    setSum(state, payload) {
      state.sum = payload;
    },
  },

  actions: {
    async submitPayment({ commit }, formData) {
      commit("setLoading", true);
      try {
        console.log(formData)
        commit('setSum', formData.sum);
        Swal()
        setTimeout(function () {
          commit('setLoading', false);
        }, 2000);
      } catch (error) {
        commit("setLoading", false);
        throw error;
      }
    },
    setLoading({ commit }, payload) {
      commit("setLoading", payload);
    },
  },

  getters: {
    loading(state) {
      return state.loading
    },
    sum(state) {
      return state.sum
    }
  }
};
